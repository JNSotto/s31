//contain all business logic

const Task = require('../models/taskSchema')

//Get All Task
module.exports.getAllTasks = () =>{
	return Task.find({}).then(result =>{
		return result
	})
}

//Create New Task

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((task,err) =>{
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

// Delete Task

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removedTask, err) => {
		if (err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

// update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}

//Activity s31

//Create a route for specific task

module.exports.getSpecificTask = (taskId) =>{
	return Task.findById(taskId).then((findTask, err) =>{
		if (err){
			console.log(err)
			return false
		} else {
			return findTask
		}
	})
}

// Create a route to change status
module.exports.updateStatus = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.status = newContent.status
			return result.save().then((updatedStatus, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedStatus
				}
			})
	})
}
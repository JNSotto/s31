// Contain all task endpoints for our applicataions

const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskControllers')

router.get('/',(req,res)=>{

	taskController.getAllTasks().then(resultFromController => res.send (resultFromController))
})

//route for creating task
router.post('/createTask',(req,res)=>{

	taskController.createTask(req.body).then(resultFromController => res.send (resultFromController))
})

//route for deleting task
router.delete('/deleteTask/:id', (req,res) =>{

		taskController.deleteTask(req.params.id).then(resultFromController => res.send (resultFromController))
})

// route for updating task

router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


//Activity s31

//Create a route for specific task

router.get('/:id', (req, res) => {

	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Create a route to change status
router.put('/:id/complete', (req, res) => {

	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


module.exports= router